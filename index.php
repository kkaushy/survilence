<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Survilence System</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="scripts/jquery-2.1.4.min.js"></script>  
    <script src="scripts/bootstrap.min.js"></script>    
    <script type="text/javascript" src="scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="scripts/main.js"></script>     
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link href="css/main.css" rel="stylesheet">
    <script type="text/javascript">
        $(document).ready(function () {
            init();
        });
    </script>

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Survilence</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">                    
                    <li>
                        <a href="record.php">Play Recordings</a>
                    </li>                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <input type="hidden" id='type' value="<?php echo $_GET['q'] ?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-4" id="cameraList">
            <?php 
                include("getcamera.php");
                $cameraList = getCameraList();
                $cameraId = isset($_GET['cameraId']) ? $_GET['cameraId'] : $cameraList[0]['cameraId'];
                echo "<h3>Camera<button type='button' id='editCamera' style='margin-left:100px;width:100px' onclick='EditCamera()' class='btn btn-success'>Edit</button></h3>";
                echo "<ul class='list-group' style='list-style:none'>";

                foreach ($cameraList as $camera) {
                    $mac = $camera['cameraId'];
                    $cameraName = $camera['name'];
                    $id = $camera['id'];
                    if($cameraId==$mac){
                        echo "<li id='cameraList'>
                                <a class='list-group-item active fixed' href='index.php?cameraId=$mac'>$cameraName</a>
                                <input type='text' id='$id' value='$cameraName' class='edit input-lg' style='display:none'/>
                            </li>";
                    }else{
                        echo "<li>
                                <a class='list-group-item fixed' href='index.php?cameraId=$mac'>$cameraName</a>
                                <input type='text' id='$id' value='$cameraName' class='edit input-lg' style='display:none'/>
                            </li>";
                    }                    
                }
                echo '</ul>';
                echo "<input type='hidden' id='cameraId' value='".$cameraId."'/>";
            ?>
            </div>
            <div class="col-lg-8 text-center">
                <div class="panel panel-default">
                    <div class="panel-body" style="height:450px">
                        <h3 id="status">Live Streamings.. </h3>
                        <div class="center" class="embed-responsive embed-responsive-16by9">
                            <video id="myVideo1" class="embed-responsive-item myvideo" preload="auto" style="display:none">
                                <source type='video/mp4'>
                            </video>
                            <video id="myVideo2" class="embed-responsive-item myvideo" preload="auto" style="display:none">
                                <source type='video/mp4'>
                            </video>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</body>

</html>
