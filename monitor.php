<?
$download_dir = '/home4/mrtechmyster/public_html/survilence/';

// init inotify
$fd = inotify_init();

// add new watch for newly created files
$watch_descriptor = inotify_add_watch($fd, $dir, IN_CREATE);
// add new watch for newly created files, using the IN_ADD mask tells inotify_add_watch
// to append to existing watch and not replace it
$watch_descriptor = inotify_add_watch($fd, $dir, IN_ADD | IN_MODIFY);

// loop and wait for events
while (1) 
{
    $events = inotify_read($fd);
    $filepath = $dir . $events['name'];

    // now we'll also have to check for the type of event
    if($events['mask'] & IN_CREATE) {
        print "New file downloaded ".$events['name']."\n";
    }
    else if($events['mask'] & IN_MODIFY) {
        print "File modified ".$events['name']."\n";
    }

}

// clean up
inotify_rm_watch($fd, $watch_descriptor);

fclose($fd);
?>