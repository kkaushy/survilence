var playList = new Array();
var playedList = new Array();
var dirName = "streamer.php?q=";
var set_interval = null;
// var cameraId = null;

// $(document).ready(init);

function init(szType){

	$('#myVideo1').hide();
	$('#myVideo2').hide();	
	
	$.get("latest.php?cameraId="+getCamera(), feed);
	
	$('#myVideo1').on('ended', videoPlay2);
	$('#myVideo2').on('ended', videoPlay1);


	document.getElementById("myVideo2").addEventListener("error", function (err) {
		videoSrc = getNextVideo();
		loadVideo("myVideo2", videoSrc);
    }, true);

    document.getElementById("myVideo1").addEventListener("error", function (err) {
		videoSrc = getNextVideo();
		loadVideo("myVideo1", videoSrc);
    }, true);


    if(szType==1){
    	playRecordings();
    }else{

    	set_interval = setInterval(function(){
			$.get("latest.php?cameraId="+getCamera(), updatePlaylist);
		}, 5000);
		$('#myVideo1').on('loadeddata', function(e){		
			videoPlay1();
		});

		$('#myVideo2').on('loadeddata', function(e){		
			videoPlay2();
		});
    }
}

function playRecordings(){

	// clearInterval(set_interval);
	// $("#status").html("Playing Recordings..");
	var startTime = $('#startTime').val();
	var endTime = $('#endTime').val();
	var cameraId = getCamera();
	$.get('recordingFiles.php',{
		"startTime":startTime,
		"endTime":endTime,
		"cameraId":cameraId
	})
	.done(function(data){
		playList = new Array();
		playedList = new Array();
		console.log("playList",playList);
		console.log("playedList",playedList);
		feed(data);					
	});
}

function videoPlay2(){	

	$('#myVideo1').hide();
	$('#myVideo2').show();

	playVideo("myVideo2");
	videoSrc = getNextVideo();
	loadVideo("myVideo1", videoSrc);
}

function videoPlay1(){				
	
	$('#myVideo2').hide();
	$('#myVideo1').show();

	playVideo("myVideo1");
	videoSrc = getNextVideo();
	loadVideo("myVideo2", videoSrc);
}

// function playActivePlayer(){
// 	console.log("playActivePlayer");
// 	if($('#myVideo2').is(':visible')){
// 		playVideo("myVideo2");
// 	}else if ($('#myVideo1').is(':visible')){
// 		playVideo("myVideo1");
// 	}	
// }

function feed(data){
	updatePlaylist(data);
	var videoSrc = getNextVideo();
	loadVideo("myVideo2",videoSrc);	
	videoPlay2();
}

function loadVideo(szElementID, videoSrc){
	objElement = document.getElementById(szElementID);
	objElement.setAttribute("src", videoSrc);
	objElement.load();
}

function playVideo(szElementID){
	objElement = document.getElementById(szElementID);
	objElement.play();
}

function changeCamera(objElement){
	
	var cameraId = $(objElement).html();
	$('#cameraId').val(cameraId);
	console.log("changeCamera",cameraId);
	init();
}

function getCamera(){
	
	cameraId = $('#cameraId').val();	
	return cameraId;	
}

function getNextVideo(){

	var videoFile = playList.shift();
	if(videoFile){
		playedList.push(videoFile);	
	}
	
	videoSrc = dirName+videoFile;
	return videoSrc;
}

function updatePlaylist(data){

	arrFiles = $.parseJSON(data);
	var cameraId = getCamera();
	for(var i in arrFiles){
		var videoFile = cameraId+"/"+arrFiles[i];
		console.log("updatePlaylist");
		console.log(videoFile);			
		if (($.inArray(videoFile, playedList) < 0) && ($.inArray(videoFile, playList) < 0) ){
			playList.push(videoFile);					
		}else{
			console.log("played");
		}
	}

	// if(playList.length>4){
	// 	console.log("clearing buffer");
	// 	temp_list = playList.splice(2,2);
	// 	for(var i in temp_list){
	// 		playedList.push(temp_list);
	// 	}
	// }
}

function EditCamera(){
	var state = $('#editCamera').html();
	if(state=="Edit"){
		$('#editCamera').html('Save');
		$('.edit').show();
		$('.fixed').hide();
		

	}else{
		var cameraList = [];
		var error = false;
		$('#cameraList').find('input:text').each(function(){
			var szId = $(this).attr('id');
			var szName = $(this).val();

			if($.trim(szName)==""){
				error = true;
			}
			cameraList.push({
				"szId":szId,
				"szName":szName
			})
		})

		if(error){
			alert("Camera Name cannot be blank");
			return false;
		}
		$.ajax({
		  type: "POST",
		  url: 'updateCamera.php',
		  data: {"cameraList":cameraList},
		  success: function(){
		 //  	$('#editCamera').html('Edit');
			// $('.edit').hide();
			// $('.fixed').show();
			location.reload(); 
		  },		  
		});
		
		

	}
}

// setInterval(playActivePlayer, 5000);	